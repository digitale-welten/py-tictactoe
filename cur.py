import sys

# # Konstanten definieren
EMPTY = " "
P_ONE = "X"
P_TWO = "O"

# # Spielbrett definieren

global board
board = [
    [EMPTY, EMPTY, EMPTY],
    [EMPTY, EMPTY, EMPTY],
    [EMPTY, EMPTY, EMPTY]
]

# # Funktionsdefinitionen

# ## Spieler
global cur_player
cur_player = P_ONE


def changePlayer():
    global cur_player

    if cur_player == P_ONE:
        cur_player = P_TWO
    else:
        cur_player = P_ONE

    return cur_player

# ## Feldauswahl


def fieldInput(axis="y"):
    eingabe = None

    while eingabe == None:
        eingabeStr = input("Bitte gib die " + axis +
                           "-Position ein, an der du setzen möchtest [1-3]: ")

        if (eingabeStr.lower() == "q"):
            print("Spiel beendet!")
            sys.exit()

        try:
            eingabe = int(eingabeStr)

            if not eingabe in [1, 2, 3]:
                raise ValueError()
        except:
            eingabe = None
            print("Bitte eine Zahl zwischen [1-3]")

    return eingabe - 1

# ## Gewinnerüberprüfung


def winner():
    global board
    if (
            # horizontale
            board[0][0] == board[0][1] == board[0][2] != EMPTY or
            board[1][0] == board[1][1] == board[1][2] != EMPTY or
            board[2][0] == board[2][1] == board[2][2] != EMPTY or
            board[0][0] == board[1][0] == board[2][0] != EMPTY or  # vertikale
            board[0][1] == board[1][1] == board[2][1] != EMPTY or
            board[0][2] == board[1][2] == board[2][2] != EMPTY or
            board[0][0] == board[1][1] == board[2][2] != EMPTY or  # diagonale
            board[0][2] == board[1][1] == board[2][0] != EMPTY
    ):
        return cur_player

    return None

# ## Volles-Brett-Überprüfung


def boardFull():
    global board

    return not EMPTY in [
        board[0][0], board[0][1], board[0][2],
        board[1][0], board[1][1], board[1][2],
        board[2][0], board[2][1], board[2][2]
    ]

# ## Brett-Ausgabe


def prettifyBoard():
    global board

    boardStr = ""
    for y in range(3):
        for x in range(3):
            boardStr += board[y][x]
            if x < 2:
                boardStr += "|"
            if x == 2:
                boardStr += "\n"
        if y < 2:
            boardStr += "-----\n"

    return boardStr

# ## Hilfsfunktionen


def fieldEmpty(y, x):
    global board

    return board[y][x] == EMPTY


def setField(y, x):
    global board, cur_player

    board[y][x] = cur_player

    return board

# ## Game-loop


def main():
    changePlayer()

    while winner() == None and not boardFull():
        print()
        print(prettifyBoard())
        print("\nSpieler " + changePlayer() + " ist an der Reihe")

        # Solange das Feld bereits besetzt ist, frag erneut
        valid_play = False
        while not valid_play:
            y = fieldInput()
            x = fieldInput("x")

            if fieldEmpty(y, x):
                setField(y, x)
                valid_play = True
            else:
                print("Bitte wähle ein leers Feld!")

    print()
    if boardFull():
        print("Unentschieden!")
    else:
        print("Spieler " + cur_player + " hat gewonnen!")

# # Ausführen


main()
