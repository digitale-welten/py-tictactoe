# 2D-array
arr = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
]

# 1D-array
arr = [
    1, 2, 3,
    4, 5, 6,
    7, 8, 9
]

# indice im 2D-array
arr = [
    0, # [0, 1, 2]
    1, # [0, 1, 2]
    2  # [0, 1, 2]
]
# -> arr[1][1] == mitte == True

# indice im 1D-array
arr = [
    0, 1, 2,
    3, 4, 5,
    6, 7, 8
]
# -> arr[4] == mitte == True
