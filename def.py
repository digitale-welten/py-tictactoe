#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 12:54:01 2020

@author: anthy
"""

# board definieren

board = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

def feldeingabeInput():
    eingabe = None
    
    while eingabe == None:
        try:
            eingabe = int(input("Bitte gib die Position ein, an der du setzen möchtest: "))
            
            if not eingabe in [1, 2, 3]:
                raise ValueError()
        except:
            eingabe = None
            print("Eine Zahl zwischen [1-3]")

    return eingabe

y = feldeingabeInput()
x = feldeingabeInput()

AKTUELLER_SPIELER = "X"
print(board[y][x])
board[y][x] = AKTUELLER_SPIELER

# Game-loop

# Spieler definieren
# Feldeingabe abfragen
# überprüfen, ob die Eingabe gültig ist (ob das Feld noch leer ist)
# wenn ja, dann setze Zeichen des Spieler in das Feld
# überprüfen, ob das Spiel vorbei ist
# 
# Spieler wechseln
# Feldeingabe abfragen
# überprüfen, ob die Eingabe gültig ist (ob das Feld noch leer ist)
# wenn ja, dann setze Zeichen des Spieler in das Feld
# überprüfen, ob das Spiel vorbei ist
#
# ...




