# Board initialisieren

"""
Feld-Bedeutungen:

Leer: " "
Spieler 1: "X"
Spieler 2: "O"

Anstelle die Strings direkt zu benutzen, könnte man sie auch Variablen zuweisen:
EMPTY = " "
PLAYER_1 = "X"
PLAYER_2 = "O"
"""

## "Per Hand"

board = [
    [" ", " ", " "],
    [" ", " ", " "],
    [" ", " ", " "]
]

## Mit for-Loops

board = []

for y in range(3):
    line = []

    for x in range(3):
        line.append(" ")
    
    board.append(line)

## Mit Generator (wie das davor, nur kürzer)

board = [[" " for x in range(3)] for y in range(3)]


# Nützliche Funktionen

def parseInput(s):
    ## Überprüft, ob die Variable s ein String ist
    if type(s) != str:
        return None

    try:
        x = int(s[0])
        y = int(s[2])

        if 1 <= x <= 3 and 1 <= y <= 3:
            return [x, y]
        
        return None
    except:
        return None

def playerInput():
    temp = None

    while temp == None:
        temp = parseInput(input("Bitte gib eine Feld-Koordinate mit folgendem Format ein [X Y] (jeweils 1-3) >> "))
    
    return temp

def checkForWinner(b = board):
    # horizontale
    if b[0][0] == b[0][1] == b[0][2] != " ":
        return b[0][0]
    elif b[1][0] == b[1][1] == b[1][2] != " ":
        return b[1][0]
    elif b[2][0] == b[2][1] == b[2][2] != " ":
        return b[2][0]

    # vertikale
    if b[0][0] == b[1][0] == b[2][0] != " ":
        return b[0][0]
    elif b[0][1] == b[1][1] == b[2][1] != " ":
        return b[0][1]
    elif b[0][2] == b[1][2] == b[2][2] != " ":
        return b[0][2]

    # diagonale
    if b[0][0] == b[1][1] == b[2][2] != " ":
        return b[0][0]
    elif b[0][2] == b[1][1] == b[2][0] != " ":
        return b[0][2]
    
    return None

"""
Anstelle zu checken, ob alle Felder voll sind, könnte man auch einfach sagen,
dass das Spiel nach spätestens 9 Zügen vorbei ist (gibt ja nur 9 Felder)
"""
def checkForBoardFull(b = board):
    return (
        b[0][0] != " " and b[0][1] != " " and b[0][2] != " "
        and b[1][0] != " " and b[1][1] != " " and b[1][2] != " "
        and b[2][0] != " " and b[2][1] != " " and b[2][2] != " "
    )

def checkForGameover(b = board):
    winner = checkForWinner(b)
    boardFull = checkForBoardFull(b)

    if winner != None:
        return winner
    
    return boardFull

def prettyPrintBoard(b = board):
    boardStr = ""

    for y in range(3):
        if y > 0:
            boardStr += "-----\n"
        for x in range(3):
            field = b[y][x]
            boardStr += field
            if x < 2:
                boardStr += "|"
        if y < 2:
            boardStr += "\n"
    
    return boardStr


# Gameloop

curPlayer = "X"

## Der while-Loop läuft so lange, wie es keinen Gewinner gibt, und das Board noch nicht voll ist
while checkForGameover() == False:
    print("Spieler ", curPlayer, " ist an der Reihe!")

    # Player input validieren -> Eines der folgenden Ausgänge (in Bezug, ob das Feld bereits belegt ist)
    ## False: Player input wiederholen
    ## True: Player input benutzen
    inputValidated = False
    while inputValidated == False:
        temp = playerInput()

        ## Player input ist eine gültige Koordinate
        if temp != None:
            x = temp[0] - 1
            y = temp[1] - 1
            if board[y][x] == " ":
                board[y][x] = curPlayer

                inputValidated = True

    # Feld anzeigen
    print()
    print(prettyPrintBoard())
    print()

    # curPlayer wechseln
    if curPlayer == "X":
        curPlayer = "Y"
    elif curPlayer == "Y":
        curPlayer = "X"

print("Game over: ", checkForGameover())
