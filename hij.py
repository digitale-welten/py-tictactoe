#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 12:33:22 2020

@author: anthy
"""

# TIPPS: 

## array definieren

arr = [1, 2, 3]
print(arr)

## element in array / liste verändern

index = 0
neuer_wert = "A"
arr[index] = neuer_wert
print(arr)
